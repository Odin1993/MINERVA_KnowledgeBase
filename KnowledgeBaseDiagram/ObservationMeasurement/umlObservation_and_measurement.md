# Contextualization of the time-series

## Sensor and measurements Representation

This part of the ontology is focused on representing the global link between sensors and their observations. This model is a subset of the general "Stimulus-Sensor-Observation" [[SSN,2011](https://www.w3.org/2005/Incubator/ssn/ssnx/ssn)]

![Figure. Sensor and measurements general representation](http://www.plantuml.com/plantuml/png/XL5BQiCm4DtFANI1NC1qLKmWAvUw3OLGZ69FgC2HKyt8rBIzU-r95Qj6fhltz3uQZOXiLGx4U5QRZQ8RooRufgdqHv83c3Lrz_NhpR4tDS8CPwb4Ivs36OR-1zTTqF4WDtH7GjSZ9No5I9fDPsDhSp_0GWw9BMhs3elJpZ8w99RSQNcw5hzUNtY--6ioyZmY-JQPP7F6QcU9jULeJj6Tf9v6nseoruoJGORhTVjFHJnZOb5xtNeCNoS4Keuf-UuRhfwjuKoG7n4abvZyjx4-SAXRJVHd3v-WX6V6lfHvUA8wV52J1VOAtt5okOTRHKtDQi4F)

## Phenomenon Representation
The term phenomenon (ssn:property) is aligned with the qualities that can be observed by a certain types of "feature of interest" (real-world entities) and Devices.

![Figure. Phenomenon Specification](http://www.plantuml.com/plantuml/png/AoxEYmyeoYz8BIgfjAdHrLN8pSnBBIfBZ7VEICtDJ4xC2SX8pSlF1U8yhc9SQbsJyu5gCfE9KUjHcLAKdvEVJfpgW8pAOcIL0000)

### Chemical Phenomenon

The chemical phenomenon refers to those natural observations related to chemical elements or procedures.

![Figure. Chemical Phenomenon detail](http://www.plantuml.com/plantuml/png/ZT312i8m30RW-zvbR-15tC6BCEm3IDT65cWIQTFAm8UttiQKsKCkoPSGVaA6MEpjD0EXis6SWOLgyU7LzxITHaZ03ZfgMvDgMQDL5Ev-TjTym9Ga5F160lh__fm9FUhIRHjhT9pgpQpGX0TuZeAik-WAeRvIsYwQ1pYye_l2-ybzk3DvIchRuxvOXOWsl040)

### Economic Phenomena

The economic phenomena makes reference to all monetary and investment aspects that could afect diferent domains (water, energy, building, etc)

![Figure. Economic Phenomenon](http://www.plantuml.com/plantuml/png/oytCIoqgIuntJSxFoy_DJ0x8I0NIG9ndKwEhgv0BbNREBoxXGkQWo7hcbQKMbm1rbU1Nv--B8a_0pe34If2HH2lFBoZDIIp9pCyZLaTGQbbgNcagiSf34eioqzAuKBaW5J2HyCIIr28qLKXYo4e3YZAJKrrp4ejBCfCpIohn6EeAfD8h8U80)

### Hydrologic phenomena
This kind of phenomena refers to hydrology domain phenomenas comprising specific variables from this domain.

![Figure. Hydrologic phenomenon](http://www.plantuml.com/plantuml/png/oytCIoqgIuptg4mfoi_9JyzC3iX8pSlF1U8ycnfTNOLSg0AVnBmKv_oor2AkN6Ar12JckPOb5kGN9PPavkSHhoyeDJaZiIWzbGGTfNd-QMd4gmykI4tE9C7yaDAYfCoKp99A4hGK9UOL5sH2_0u0)

### Physic Phenomenon

This phenomenom refers to the physical aspects that can be observed in the environment.

![Figure. Physic Phenomenon](http://www.plantuml.com/plantuml/png/dPLBRy8m3CVl_HIS9o6n-ovxC6q7j0ewjXiAYQ6MyYAFQaZxy4jX3wLenUNGLd5-_iTExCPp9c5qFPbChjfNeL23QzYghFSUULc3DYez-kPpF1xzJDuwO3Jodbd6CUoBhY6ZIcT4v06DFk7xrOqqZehgmn8DMVm1j1y0sr0NsIYRd-uYDccdTSZ9Vb5qSDWL8lxoJWEdwRFzI-mup0cP5dR8pvDFmCAZZ1yTid_zXbdZ63spQME2OzfR7E8r0u5H4TdM5gXqcSu5BOO1mIGNxwC36Zrd0dFnNql6FMkE8WL-of_Cv0syziQjGLpgFuTDMo5_0kx201QSXG3kyaYgjYuKXj0dyKyIhLLpABg6JXDwxY30mhOj4LKrw-UhsHtJefzvrRWnJgNqlqMfLO5wmwGaqiSp6l37MzJC4Wiq1Gw2XbQWBAI-FLlykoZ2ga953E5myfOvq9oOtnAaeLykI_IuHaddZOpKsDzO0-aos8Rw2m00)

#### Physic Phenomenon: Medium

The "medium" phenomenon and concept refers to the observed medium (water, air, etc).

![Figure. Media detail regarding physic phenomenon](http://www.plantuml.com/plantuml/png/AqvEp4bLC3GmCD024LovcNcfHMM9yRwfAPcbkJOrkheAC277ajpyueACrA9KY2G-jUv5-QLvAS5v-UK4rOOdbgGMeQf24a8nDI2_A9K4vS50eTpKl18Ij7Zc0SMBIp9BCXDpoBKfbpWjfKLfYSdOl8CbZ1mpqTJ01J321qCfjkW9pi_CGLE54C9GYMaaTkkngyLZC3RFumml7EgnA0t9B2ukJSqXh30qBnkOGlCWCW00)

#### Physic Phenomenon:Shape

This specific phenomena represent those phenomenos related with the dimensions.

![Figure. Shape detail regarding physic phenomenon](http://www.plantuml.com/plantuml/png/oytCIoqgIumFpaWiIBMfqTLLo8MAE1Vb5nS7foQNPERdSUNYLEQHcfcUKO97WKzgNdf91Xu5mJdv1QbuvCC9sH2UcLAI0G00)

#### Physic Phenomenon: Volume

This specific phenomena refers to the inflows and outflows that can be oberserved in a system or the environment.

![Figure. Volume detail regarding physic phenomenon](http://www.plantuml.com/plantuml/png/oytCIoqgIumFoyyfpKsrgT7LLSY52hba5YTd91QbftBbubRabfDVpeNCGP7rp4l3A-zVMW9I0000)
