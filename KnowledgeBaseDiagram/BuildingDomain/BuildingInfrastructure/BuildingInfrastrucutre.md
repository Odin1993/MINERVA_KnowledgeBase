# Building Infrastructure

This part of the semantic model is focused on the representation and contextualization of the building elements. This part comprise the representation of buildins monitoring and control device as well as infrastructural elements.

## Sensors and Actuators for Buildign domain
One part of the building infrastructure part of the building is composed by sensors and actuators that performs some deicions over specific zones.

For that, the following figure shows a specific categorization of the building devices (sensors and Actuators).

![Figure. Sensors and Actuators Categorisation](http://www.plantuml.com/plantuml/png/ZP9DQm8n48Rl-oiy5uIjPo-jsbfG52pqA6bsN8ScCqioQH7wuviLjUo2oLv2c7dcwvtnMXaOFPPb-T0-nH5bKgjc_tRGpm5DXLJFcCInyNEs2cdwEnwFlAVz3iYpA_w_PyyrVhQPf84NT90X3F-aYLVbqy0IbAI9DQENa6jbpTzWWJAvjWwyp-RQMVxA8HoeIYCB0bUVB0ZS7pvOh51EwKJlO1jmAg_1AgOZHJg3RRGEJSGoPJ_mDY4UT7jkyxW8Xvz1aEboSWjpLhqJUFSm1vxkbNhI4fJ4ydr3IzqM0R9b92dwXguoM1ybLXj6TmJl1Gnh_U8ouFu0)

## Feature of Interest for Building domain

In terms of Feature of Interest and concretely for the building domain, the feature of interests are subdivided in:
* Electric Elements: Elements regarding the electric part of the building.
* Mechanic Elements: Elements that take part of the mechanical part of the building (HVAC, plumbing, etc).
* InfrastructureElements: Elements that take part of the building but are not included in any electric or mechanic part (Zones, Furniture, etc)

![Figure.Feature Of Interest Categorisation](http://www.plantuml.com/plantuml/png/bT2n2i9030RWlK_H5-ZYxAHOw20L-W0bfBasSBqUkPoJ3syhLLGAdbCW-VuG4e9hImADGhMfd99Gq7n_Aufywh5jOFIMkbM4B2HmTltBRpNVUFcS7QDSALbV9dQQh9l9SfUkenZ0z5iQ3yfWl-ZVSK_88xbiu-cFrC2wwy6tXyYsMtRMLRRH-x0dIwZ2EDTvjVxqPy81NBgld148AX6NEzVK7G00)

### Electric Elements
The electric elements corresponds with the audio visual, electric and communication appliances.

![Figure. Electric Elements Categorisation](http://www.plantuml.com/plantuml/png/oyWiCKfFB8XtAitCISdCItVpzxIfqTLLo82Ak-Qa9fSKPIO3wTpKl18k7E8AA9gSytDpI_CoanDBClFp70iASZ8Jyv9JYTCBamHhIyQg3o65gjKdCRU0E4iSIrCoyyCoYqiJSqXqNd1gScbHPaabN1i0)

### Mechanic Elements

The mechanical components are part of the building like plumbing, HVC system, etc.

![Figure. Mechanic Elements Categorisation](http://www.plantuml.com/plantuml/png/oyWiCKfFB8XtAitCISdCItVpzxIfqTLLo82A-wOcPoJcPIQxvgJcfkQLSE4GHz7Z4UReZAuUIGn5hLD-Pavg4K5bplcv1KMfnSNvX9Mw9UOHLEEHcbY2P2i-pmJabEOc0GC6AWS0)

### Infrastructure Elements

This part of the building is focused to describe open spaces, zones and furniture elements of the building that has to be considered for building-user interaction.

![Figure. Infrastructure Elements Categorisation](http://www.plantuml.com/plantuml/png/bPDDQyCm38Rl_HKv3mhPpZkLBhIdZ0LMsALejj8A7BauyWQZFxxnO2msmruk1adFQttPemA3rNrTrtVXM9r97avmEMuy6KryQjhzutMzhdxyU-uTZEAy4k_moU20BAjiDD8ttZ65u0BDc-LVF1ZLTxXAQyqn3IWinJll-WAsiuw-B0kOgAZOdGni93tlDfWHqVOzATo1CJdgnThXF_bQBr72FvfuHIUaaXRAQzWOOhs0dtmV8DOja1pcGmX6Uc5GeCvZpyOFxzDpo5ET06jmXRkwCqACb9jeByXJ-lHlnEv8iRMpAMMumxG0-ta3)
