# Recommendation Representation
This part of the semantic model is focused on representing the recommendations related to non-desirable states.  

![Figure. Recommendations Representation](http://www.plantuml.com/plantuml/png/oytCIoqgIumFBaaiILNGrLN8XGe4fIRdv-QcvgKabcJcvrd1nJCIYr6bkB3hKB2fGJBDgJGp9oKpBvqChU50XnMHeSypBwMqkAIe4bKD0000)


## Building State Recommendations

Building complex state recommendations refers to the strategy to overcome the non-desirable complex state in the building. 

![Figure. Building Complex State Recommendations](http://www.plantuml.com/plantuml/png/oytCIoqgIuntAitCISdCImzAJSxFpKtDIqaioSpFiwdHrLN8o2mnIayiY7VEpqtBBof1LS6LIxm1ha2LwPMkQMcPoPcfUScLg4e1)


## Water Complex State Recommendations
TBD

## Industry Complex State Recommendations
TBD

