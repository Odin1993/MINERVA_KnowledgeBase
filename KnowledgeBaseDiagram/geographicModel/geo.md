# Geographic representation of the entities

This package is focused on linking the SSN module with geographic contextualisation of the information. This ontology will support in the future the geospatial reasoning over the elements of the domain-specific ontologies.

![Figure. Geographic representation of the elements and Feature Of Interest](http://www.plantuml.com/plantuml/png/Iq_DZtTFpSzDBIcgLD3LLKW7yjrI4qjAYrAjGEoCn6AOD1UId89D3L1nSN5UV71bSKbgBdOvg81_ccTUIMfHQd49Lt9EOd4ns1AOgTLS2Y2-5T0UNn-5Q10pFJV79nEe8J4d5YuPxXq2aIpFBe5BrW80)
