# Semantic Representation of diverse states

The states refer to the current state of the domain or parts of the domain. It could make reference to complex states (comfort in a building, energy inefficiency, lack of quality, etc) or Simple states (specific elements state as on/off). 


![Figure. State Representation](http://www.plantuml.com/plantuml/png/AoxEYm-kB2v9pLNGrLN8pSnBBIfBZ0ykIIn9jO9pCnABmI9SAF8ADZL8MfppSmjoKYi8gWlE1Aa3Im00)

## Complex State Representation

The “Complex State” of a domain represents rule-derived states or restriction derived states that combine different values to determine this state. Hence, this state representation has been subdivided into: 

- *Building Complex State*: To make reference with a combination of simple situations that generate an overall state in the building.
- *Water Infrastructure  Complex State*: To make reference with a combination of simple situations that generate an overall state in the water infrastructure.
- *Industry Infrastructure Complex State*: To make reference with a combination of simple situations that generate an overall state in the industry infrastructure and processes.

![Figure. Complex State Representation](http://www.plantuml.com/plantuml/png/oytCIoqgIuntpiyjo4cj22v9B4bLiAdHrLN8XSeubMRcf6JcfIEhuCebg2qSA5BacPTMb5XSKbIQN59Qb4gY2PvvAQL0lPMu3G40)

### Building State
The building state is defined as facilities states that are the result of combining multiple information. For example, an energy inefficient state in the building could be denied from the combination between the occupancy (no-one in the room) and the current situation of the room (lights switched on).

![Figure. Building Complex State Representation](http://www.plantuml.com/plantuml/png/oytCIoqgIuntAitCISdCItVEpot8IQq8BaaiILMmgT7LLSZ8B35AJoo81ygavHULW6Mui0cYgFPFJYujICmhWIeWNaDeNZ8UBQXESWMwFRtIDIqjCpapDI-v4jLjM6Nn6L12fdxFl5Hy9g1BOpF2CIy_Dp6daWoTuOd59S1u0m00)

### Water Infrastructure Complex State
TBD

### Industry Infrastructure Complex State
TBD

## Simple State
The simple state refers to those simple or specific situations that determine the current “state” of an element. Similarly as complex state, this kind of state also has been subdivided in different domains: 

- *Building Simple State*: To make reference to the building elements specific state 
- *Water Infrastructure  Simple State*: To refer the state of the water infrastructure even or not connected with the rest of domains. 
- *Industry Infrastructure Simple State*: To define the Industry or process states.

![Figure. Simple State Representation](http://www.plantuml.com/plantuml/png/oytCIoqgIumFpimjo4aDBaaiILMmgT7LLSX5aN0gpSn9oSnB1olWoYLEKpXGl8Wa7PvvAQN59KMLO0K0)

### Building Simple State

The building simple state refers to facilities specific situations such as swished on/off, occupancy or empty in the room, etc. 

![Figure. Building Simple State Representation](http://www.plantuml.com/plantuml/png/bT6n2eCm58NXlK_XN11YeLkNgeKE1GTf2rr4p5Kln2Heh1Jwy5K7DMWnhYVVy9DR9J43opaIidS88Ri2EPJlE0gna0pS6Xb5deKgLc2TlhPjvQWEMImZbo6dLV-mMzZczYxDd2yQRfxX2r7g4SEYkK02xrvMcCT41RqpBhW-pfvUC2pBBk36CaqtT40xbKObernlcTWjDS_sagBHQoRRWkqkP4ZxAr7pc1jckT9RnkcFdFpCkjtoCo_1GMyPbfcx559z35oG9BL4e3y0)

### Water Infrastructure Simple State
TBD

### Industry Infrastructure Simple State
TBD


